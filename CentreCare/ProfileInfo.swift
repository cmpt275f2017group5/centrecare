//
//  ProfileInfo.swift
//  CentreCare
//
//  Created by Jason Trinh on 2017-11-30.
//  Copyright © 2017 CenterCare. All rights reserved.
//

/// Removed, not storing patient information on the phone anymore

import UIKit
import os.log

class ProfileInfo: NSObject, NSCoding {
    
    //MARK: Properties
    
    var email: String
    var firstname: String
    var lastname: String
    var phonenumber: String
    var street: String
    var postal: String
    var country: String
    var province: String
    var city: String
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("profiles")
    
    //MARK: Types
    
    struct PropertyKey {
        static let email = "email"
        static let firstname = "firstname"
        static let lastname = "lastname"
        static let phonenumber = "phonenumber"
        static let street = "street"
        static let postal = "postal"
        static let country = "country"
        static let province = "province"
        static let city = "city"
    }
    
    //MARK: Initialization
    
    init?(email: String, firstname: String, lastname: String, phonenumber: String, street: String, postal: String, country: String, province: String, city: String) {
        
        // The name must not be empty
        guard !firstname.isEmpty else {
            return nil
        }
        
        
        // Initialize stored properties.
        
        self.email = email
        self.firstname = firstname
        self.lastname = lastname
        self.phonenumber = phonenumber
        self.street = street
        self.postal = postal
        self.country = country
        self.province = province
        self.city = city
        
    }
    
    //MARK: NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(email, forKey: PropertyKey.email)
        aCoder.encode(firstname, forKey: PropertyKey.firstname)
        aCoder.encode(lastname, forKey: PropertyKey.lastname)
        aCoder.encode(phonenumber, forKey: PropertyKey.phonenumber)
        aCoder.encode(street, forKey: PropertyKey.street)
        aCoder.encode(postal, forKey: PropertyKey.postal)
        aCoder.encode(country, forKey: PropertyKey.country)
        aCoder.encode(province, forKey: PropertyKey.province)
        aCoder.encode(city, forKey: PropertyKey.city)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        // The first name is required. If we cannot decode a name string, the initializer should fail.
        guard let firstname = aDecoder.decodeObject(forKey: PropertyKey.firstname) as? String else {
            os_log("Unable to decode the first name for a profileinfo object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        
        let email = aDecoder.decodeObject(forKey: PropertyKey.email) as! String
        let lastname = aDecoder.decodeObject(forKey: PropertyKey.lastname) as! String
        let phonenumber = aDecoder.decodeObject(forKey: PropertyKey.phonenumber) as! String
        let street = aDecoder.decodeObject(forKey: PropertyKey.street) as! String
        let postal = aDecoder.decodeObject(forKey: PropertyKey.postal) as! String
        let country = aDecoder.decodeObject(forKey: PropertyKey.country) as! String
        let province = aDecoder.decodeObject(forKey: PropertyKey.province) as! String
        let city = aDecoder.decodeObject(forKey: PropertyKey.city) as! String
        // Must call designated initializer.
        self.init(email: email, firstname: firstname, lastname: lastname, phonenumber: phonenumber, street: street, postal: postal, country: country, province: province, city: city)
        
    }
}

