//
//  UniversalLogin.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/10/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material
import SwiftHEXColors
import Alamofire

var userID : String = ""
var userType : String = ""

//The details of the postions and height and width of each button
struct ButtonLayout {
    
    //Login button for button in Universal Login Page
    struct loginButton {
        static let width: CGFloat = 300
        static let height: CGFloat = 44
        static let offsetY: CGFloat = 150
    }
    
    //SignUp button for the button in Universal Login Page
    struct signUp {
        static let width: CGFloat = 150
        static let height: CGFloat = 44
        static let offsetY: CGFloat = 200
    }
    
    //Patient button for the button in Account Type Page
    struct patientButton {
        static let width: CGFloat = 130
        static let height: CGFloat = 50
        static let offsetY: CGFloat = 0
        static let offsetX: CGFloat = 90
    }
    
    //Caretaker button for the button in Account Type Page
    struct caretakerButton {
        static let width: CGFloat = 130
        static let height: CGFloat = 50
        static let offsetY: CGFloat = 0
        static let offsetX: CGFloat = -90
    }
    
    //Edit Button for the button in Maps Page
    //Need to be moved to maps page to be clear where the button is coming from
    struct editButton {
        static let width: CGFloat = 70
        static let height: CGFloat = 50
        static let offsetY: CGFloat = -270
        static let offsetX: CGFloat = 290
    }
    
    //Set Geofence Button for the button in the Maps Page
    //Need to be moved to maps page to be clear where the button is coming from
    struct setGeofenceButton {
        static let width: CGFloat = 300
        static let height: CGFloat = 50
        static let offsetY: CGFloat = 300
        static let offsetX: CGFloat = 0
    }
    
    struct emergencyContactProfileButton{
        static let width: CGFloat = 300
        static let height: CGFloat = 50
        static let offsetY: CGFloat = -140
        static let offsetX: CGFloat = 0
    }
    

}

//This is where the user can login. If the credentials are correct, the user is then directed to the respective page of which
//the account type. If the account type is caretaker, then the user goes to the caretaker menu.
//If the account type is patient, then the user goes to the patient menu
class UniversalLogin: UIViewController {
   
    //Fileprivate restricts the variable to this source file
    //Added for security to prevent the usernameField being access outside the file
    fileprivate var usernameField: TextField!
    fileprivate var passwordField: TextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            print("Running on Similator")
        #endif
        
        //Sets the background color of the page
        view.backgroundColor = UIColor(hex: 0xa8c3e2)
        
        //Loads the objects on the pages
        prepareUsernameField()
        preparePasswordField()
        prepareLogoImage()
        prepareLoginButton()
        prepareSignUpButton()
 
        //Looks for tap on the screen.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UniversalLogin.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func EmergencyContactTest(_ sender: Any) {
        //Goes to the Account Type view
        let storyBoard: UIStoryboard = UIStoryboard(name: "PatientList", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PatientList") as! PatientList
        self.present(newViewController, animated: true, completion: nil)
    }
        
    //Action occurs when the login button is pressed
    //It check if the login exist
    internal func login(button: UIButton) {
        checkLoginAlomafire()
    }
    
    //Action occurs when the signUp button is pressed
    //Loads the account type view to select the type of account it will like to create an account for
    func goToSignUpPage(sender: UIButton){
        
        //Goes to the Account Type view
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AccountType") as! AccountType
        self.present(newViewController, animated: true, completion: nil)
    }
    
}

extension UniversalLogin {
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    fileprivate func prepareUsernameField() {
        usernameField = TextField()
        usernameField.placeholder = "Username"
        usernameField.font = UIFont (name: "Helvetica Neue", size: 25)
        usernameField.clearButtonMode = .whileEditing
        
        view.layout(usernameField).center(offsetY: -50).left(20).right(20)
    }

    fileprivate func preparePasswordField() {
        passwordField = TextField()
        passwordField.placeholder = "Password"
        passwordField.font = UIFont (name: "Helvetica Neue", size: 25)
        passwordField.clearButtonMode = .whileEditing
        passwordField.isVisibilityIconButtonEnabled = true
        
        view.layout(passwordField).center(offsetY: 50).left(20).right(20)
    }
    
     func prepareLogoImage(){
        let imageName = "appicon"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        imageView.center = self.view.center
        imageView.frame = CGRect(x: ((UIScreen.main.bounds.width) * 0.5) - 50, y: (UIScreen.main.bounds.height) * 0.1 , width: 100, height: 100)
        view.addSubview(imageView)
    }
    
    
    fileprivate func prepareLoginButton() {
        let loginButton = RaisedButton(title: "Login", titleColor: .white)
        loginButton.pulseColor = .white
        loginButton.backgroundColor = Color.blue.lighten1
        loginButton.addTarget(self, action: #selector(login), for: .touchUpInside)
        
        view.layout(loginButton)
            .width(ButtonLayout.loginButton.width)
            .height(ButtonLayout.loginButton.height)
            .center(offsetY: ButtonLayout.loginButton.offsetY)
    }
    
    fileprivate func prepareSignUpButton(){
        let signUpButton = RaisedButton(title: "Sign Up Here", titleColor: .white)
        signUpButton.pulseColor = .white
        signUpButton.backgroundColor = Color.purple.lighten1
        signUpButton.addTarget(self, action: #selector(goToSignUpPage), for: .touchUpInside)
        
        view.layout(signUpButton)
            .width(ButtonLayout.signUp.width)
            .height(ButtonLayout.signUp.height)
            .center(offsetY: ButtonLayout.signUp.offsetY)
        
    }
    
    func checkLoginAlomafire(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        let url = URL(string: baseURL + "api/session")!
        
        print(url)
        
        let parameters = ["username": usernameField.text, "password": passwordField.text]
        //let parameters = ["username": "asdfasdf", "password": "secret"]
        //let parameters = ["username": "Asdasdalsjdjljkasd", "password": "alskdalksdjlas"]
        
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
    
        //print(response.response?.allHeaderFields)
        if let xID = response.response?.allHeaderFields["X-PID"] as? String {
            // use contentType here
            userID = xID
        }
        
        if let xID = response.response?.allHeaderFields["X-CID"] as? String {
            // use contentType here
            userID = xID
        }
           switch response.result {
               case .success:
                    
                   //Transfering the responce to JSON and assigning the value
                    
                    if let result = response.result.value {
                        let JSON = result as! NSDictionary
                        //Set access token to sessionID which is a global set in appdelegate.swift
                        sessionId = JSON["access-token"] as! String

                        //Set the user type which is set global in this file
                        userType = JSON["user-role"] as! String
                        
                        print(userType)

                        if(userType == "CARETAKER"){
                            let storyBoard: UIStoryboard = UIStoryboard(name: "PatientList", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "PatientList") as! PatientList
                            self.present(newViewController, animated: true, completion: nil)
                        }

                        if(userType == "PATIENT"){
                            let storyBoard: UIStoryboard = UIStoryboard(name: "PatientScreen", bundle: nil)
                            let newViewController = storyBoard.instantiateViewController(withIdentifier: "PatientMenu") as! PatientMenu
                            self.present(newViewController, animated: true, completion: nil)
                        }
                }
            
                case .failure:
                    // create the alert
                    let alert = UIAlertController(title: "Login Error", message: "Invalid username or password", preferredStyle: UIAlertControllerStyle.alert)
                    
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
}



