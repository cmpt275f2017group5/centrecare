//
//  QRCodeReaderPage.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/13/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

var pairingCode : String = ""

//This page uses a camera to read the QR code
class QRCodeReaderPage: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var square: UIImageView!
    var video = AVCaptureVideoPreviewLayer()
    
    @IBAction func Back(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AccountType") as! AccountType
        self.present(newViewController, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Creating session
        let session = AVCaptureSession()
        
        //Define capture devcie
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do{
            let input = try AVCaptureDeviceInput(device: captureDevice)
            session.addInput(input)
        }
        catch{
            print ("ERROR")
        }
        
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        output.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        let screenSize = UIScreen.main.bounds
        video.videoGravity = AVLayerVideoGravityResizeAspectFill
        video.frame = CGRect (x:0, y:screenSize.height * 0.15, width: screenSize.width, height: screenSize.height * 0.85)
        view.layer.addSublayer(video)
        
        self.view.bringSubview(toFront: square)
        
        session.startRunning()
    }
    
    
    
    //Once the QR has been read, it is displayed as a alert
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        if metadataObjects != nil && metadataObjects.count != 0
        {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
            {
                if object.type == AVMetadataObjectTypeQRCode
                {
                    let alert = UIAlertController(title: "QR Code", message: object.stringValue, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Retake", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
                        print("you have pressed the ok button")
                        
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CreateAccount") as! CreateAccount
                        self.present(newViewController, animated: true, completion: nil)
                    }))
                    present(alert, animated: true, completion: nil)
                    print(object.stringValue)
                    
                    pairingCode = object.stringValue
                    
                }
            }
        }
    }

    
}
