//
//  PatientMenu.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/16/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit


//Displays all the options for the paitient
class PatientMenu: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocationService.sharedInstance.startUpdatingLocation()

    }
    @IBOutlet weak var logOutPatient: UIButton!

    //When the patient logs out, it goes back to login
    @IBAction func logOutPatient(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "UniversalLogin") as! UniversalLogin
        self.present(newViewController, animated: true, completion: nil)
    }
    
    //When the paitient wants to pair the account with caretaker, it takes them to the QRcode reading screen
    @IBAction func pairingPatient(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "PatientScreen", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "QRCodeReaderPage") as! QRCodeReaderPage
        self.present(newViewController, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
