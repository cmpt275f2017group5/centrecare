//
//  ProfileList.swift
//  CentreCare
//
//  Created by Jason Trinh on 2017-11-30.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import os.log
import Alamofire


var patientIDGlobal: String  = ""

// Entire Patient List is shown here
class PatientList: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    //Initializing variables
    var tableView: UITableView = UITableView()
    //Storing the patients names into this list
    var patientList : [String] = []
    //Store their unique ID that is used to query from database
    var patientIDList : [Int] = []
    //Removed storing on device
    var patients = [ProfileInfo]()
    
    //var pateintList : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getPatientList()
    
        //NAVIGATION, Add button
        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height:50))
        let navItem = UINavigationItem(title: "Patients")
        let addItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(addButton))
        navItem.rightBarButtonItem = addItem
        navBar.setItems([navItem], animated: false)
        self.view.addSubview(navBar)
        
        
        
        //Constructing tableView.
        let yOffset = CGFloat(50.0)
        self.tableView.frame = CGRect(x:0, y: self.view.frame.origin.y + yOffset, width: self.view.frame.width, height: self.view.frame.height * 0.70);
        

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = 50
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.sectionHeaderHeight = 50
        self.view.addSubview(self.tableView)
    }
    
    
    //Goto Notifications Page
    @IBAction func Notifications(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "Notifications") as! Notifications
        self.present(newViewController, animated: true, completion: nil)
    }
    
    //Goto Profiles Page
    @IBAction func ProfileButton(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProfilePage") as! ProfilePage
        self.present(newViewController, animated: true, completion: nil)
    }
    //Linked on storyboard
    @IBAction func SettingsButtons(_ sender: Any) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patientList.count
    }
    
    //Display the Patients Names List
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
        cell.textLabel?.text = patientList[indexPath.row]
        print(indexPath.row)
        return cell
    }
    
    //Present patient's profile page on selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("Num: \(indexPath.row)")
        //print("Value: \(patientList[(indexPath.row)])")
        
        print(patientIDList[indexPath.row])
        patientIDGlobal = String(patientIDList[indexPath.row])
        //print(patientIDGlobal)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CaretakerPatientMenu") as! CaretakerPatientMenu
        self.present(newViewController, animated: true, completion: nil)
    }
    
    //Editting the table, swipe to delete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            // remove the item from the patientList model
            patientList.remove(at: indexPath.row)
            
            // delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            
        } else if editingStyle == .insert {
            tableView.insertRows(at: [indexPath], with: .fade)
            // Not used in our example, but if you were adding a new row, this is where you would do it.
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Monitoring \(patientList.count)/10 patients"
    }
    
    //Objc back function that unwinds to previous view controller
    @objc func goBack(){
        dismiss(animated: true, completion: nil)
        NSLog("back pressed")
    }
    //Removed
    @objc func edit() {
        NSLog("edit pressed")
        patientList.append("new Test Patient \(patientList.count)")
        NSLog("\(patientList[patientList.count-1])")
    }
    //addButton function and goto patient profile screen
    @objc func addButton(sender: UIBarButtonItem) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "PatientScreen", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "QRCodeGeneratorPage") as! QRCodeGeneratorPage
        self.present(newViewController, animated: true, completion: nil)
        
    }
    //Removed unwinding into this viewcontroller
    @IBAction func unwindToPatientList(sender: UIStoryboardSegue) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            print(patientList[selectedIndexPath.row])
        }
        else{
            let newIndexPath = IndexPath(row: patientList.count, section: 0)
            print (newIndexPath)
            patientList.append("new patient again")
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }
        
        
        
    }
    //Removed
    private func savePatients() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(patients, toFile: ProfileInfo.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Patients successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save patients...", log: OSLog.default, type: .error)
        }
    }
    //Removed
    private func loadPatients() -> [ProfileInfo]?  {
        return NSKeyedUnarchiver.unarchiveObject(withFile: ProfileInfo.ArchiveURL.path) as? [ProfileInfo]
    }
    
}
extension PatientList{
    //Get PatientList from the database and append to the patient list
    func getPatientList(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string: baseURL + "api/caretaker/" + userID + "/plist")!
        
        print(url)
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
           print(response)
            switch response.result {
            case .success:
                
                //Transfering the responce to JSON and assigning the value
                
                if let result = response.result.value as? NSArray{
                    print(type(of: result))
                    
                    for item in result { // loop through patientList items
                        print(item)
                        //append to patient list
                        self.patientList.append("new Test Patient \(item)")
                        self.patientIDList.append(item as! Int)
                    }
                    //reload the table after appending to the list
                    self.tableView.reloadData()
                }
                
            case .failure:
                print("failed getting patientlist")
            }
        }
    }
    //Load the patients after getting the array of patients from the database
    func loadPatientToTable(patientID: String){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string: baseURL + "api/patient/" + patientID + "/profile")!
        
        print(url)
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response)
            switch response.result {
            case .success:
                
                //Transfering the responce to JSON and assigning the value
                
                if let result = response.result.value {
                    //let JSON = result as! NSDictionary
                    print(result)
                }
                
            case .failure:
                print("failed getting patientlist")
            }
        }
    }
    
    
}
