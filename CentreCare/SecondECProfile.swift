//
//  SecondECProfile.swift
//  CentreCare
//
//  Created by Swimm Chan on 2017-11-27.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit

class SecondECProfile: UIViewController {
    
    //var currentInfo: [String] = SecondEC

    override func viewDidLoad() {
        super.viewDidLoad()

        let First = UILabel()
        let Last = UILabel()
        let PhoneNumber = UILabel()
        let Address = UILabel()
        let Relationship = UILabel()
        
        First.text = SecondEC[0]
        Last.text = SecondEC[1]
        PhoneNumber.text = SecondEC[2]
        Address.text = SecondEC[3]
        Relationship.text = SecondEC[4]
        
        First.font = UIFont (name: "Helvetica Neue", size: 20)
        view.layout(First).center(offsetY: -90).left(155).right(20)
        
        Last.font = UIFont (name: "Helvetica Neue", size: 20)
        view.layout(Last).center(offsetY: -30).left(155).right(20)
        
        PhoneNumber.font = UIFont (name: "Helvetica Neue", size: 20)
        view.layout(PhoneNumber).center(offsetY: 30).left(185).right(20)
        
        Address.font = UIFont (name: "Helvetica Neue", size: 20)
        view.layout(Address).center(offsetY: 90).left(130).right(20)
        
        Relationship.font = UIFont (name: "Helvetica Neue", size: 20)
        view.layout(Relationship).center(offsetY: 160).left(155).right(20)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
