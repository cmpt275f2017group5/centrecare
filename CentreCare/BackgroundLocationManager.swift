import UIKit
import CoreLocation
import Alamofire
//Sends the patient location while the patient application is running in the background
public class LocationService: NSObject, CLLocationManagerDelegate{
    
    public static var sharedInstance = LocationService()
    let locationManager: CLLocationManager
    var locationDataArray: [CLLocation]
    var useFilter: Bool
    
    
    override init() {
        locationManager = CLLocationManager()
        
        //THis is what use to make sure the batterylife doesnt drain as fast
        //locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        
        //This is how much should the distance (meters) have been moved before things are updated
        locationManager.distanceFilter = 1
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationDataArray = [CLLocation]()
        
        useFilter = false
        
        super.init()
        
        locationManager.delegate = self
        
        sendLocationData(longitude: 1982739.000, latitude: 19283721.000)
        
        
    }
    
    
    func startUpdatingLocation(){
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }else{
            //tell view controllers to show an alert to turn on background location
            showTurnOnLocationServiceAlert()
        }
    }
    
    
    //MARK: CLLocationManagerDelegate protocol methods
    public func locationManager(_ manager: CLLocationManager,didUpdateLocations locations: [CLLocation]){
        
        if let newLocation = locations.last{
            print("new Location")
            print("(\(newLocation.coordinate.latitude), \(newLocation.coordinate.latitude))")
            
            var locationAdded: Bool
            if useFilter{
                locationAdded = filterAndAddLocation(newLocation)
            }else{
                locationDataArray.append(newLocation)
                locationAdded = true
            }
            
            
            if locationAdded{
                notifiyDidUpdateLocation(newLocation: newLocation)
            }
            
        }
    }
    
    func filterAndAddLocation(_ location: CLLocation) -> Bool{
        let age = -location.timestamp.timeIntervalSinceNow
        
        if age > 10{
            print("Locaiton is old.")
            return false
        }
        
        if location.horizontalAccuracy < 0{
            print("Latitidue and longitude values are invalid.")
            return false
        }
        
//        if location.horizontalAccuracy > 100{
//            print("Accuracy is too low.")
//            return false
//        }
        
        print("Location quality is good enough.")
        locationDataArray.append(location)
        
        return true
        
    }
    
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        if (error as NSError).domain == kCLErrorDomain && (error as NSError).code == CLError.Code.denied.rawValue{
            //User denied your app access to location information.
            showTurnOnLocationServiceAlert()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        if status == .authorizedWhenInUse{
            //You can resume logging by calling startUpdatingLocation here
            startUpdatingLocation()
        }
    }
    
    func showTurnOnLocationServiceAlert(){
        print("turn on location service alert")
        NotificationCenter.default.post(name: Notification.Name(rawValue:"showTurnOnLocationServiceAlert"), object: nil)
    }
    
    func notifiyDidUpdateLocation(newLocation:CLLocation){
        print("notifiyDidUpdateLocation")
        print(newLocation.coordinate.latitude)
        print(newLocation.coordinate.longitude)
        sendLocationData(longitude: 1982739.000, latitude: 19283721.000)
        NotificationCenter.default.post(name: Notification.Name(rawValue:"didUpdateLocation"), object: nil, userInfo: ["location" : newLocation])
    }
    
    func sendLocationData(longitude: Double, latitude: Double){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string: "http://centrecare.org/api/patient/" + userID + "/location")!
        
        let parameters = ["longitude": longitude, "latitude": latitude]
        //let parameters = ["username": "ron", "password": "secret"]
        
        Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
           
            
           
            switch response.result {
            case .success:
                print("success")
                //Transfering the responce to JSON and assigning the value
                
//                if let result = response.result.value {
//                   
//                }
                
            case .failure:
                print("failure")
               
            }
        }
    }
}

