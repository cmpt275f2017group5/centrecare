//
//  AddEmergencyContact.swift
//  CentreCare
//
//  Created by Swimm Chan on 2017-11-21.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material
import SwiftHEXColors
import Alamofire

var EntryArray:[String] = []
var SecondEC:[String] = []
var ThirdEC:[String] = []

class AddEmergencyContact: UIViewController {

    
    fileprivate var FirstNameField: TextField!
    fileprivate var LastNameField: TextField!
    fileprivate var PhoneNumberField: TextField!
    fileprivate var Email: TextField!
    
    fileprivate var firstname : String = "";
    fileprivate var lastname : String = "";
    fileprivate var phonenumber : String = "";
    fileprivate var email : String = "";
    
    //var EntryArray:[String] = []
    
    //screen stuff should set up here first
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(emergencycontactIDGlobal == ""){
            prepareFirstNameField(firstname: firstname)
            prepareLastNameField(lastname: lastname)
            preparePhoneNumberField(phonenumber: phonenumber)
            prepareAddressField(email: email)
        }
        else{
            getEmergencyContactList()
        }

        

        //Looks for tap on the screen.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UniversalLogin.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Perform Action
    @IBAction func addEmergencyContact(_ sender: Any) {
        addEmergencyContact()
    }
    
    @IBAction func BackButton(_ sender: Any) {
        
    }
}

extension AddEmergencyContact{
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    fileprivate func prepareFirstNameField(firstname: String){
        FirstNameField = TextField()
        FirstNameField.placeholder = "Enter First Name"
        FirstNameField.font = UIFont (name: "Helvetica Neue", size: 20)
        FirstNameField.text = firstname
        FirstNameField.clearButtonMode = .whileEditing
        
        //FirstNameField.isVisibilityIconButtonEnabled = true //hides the content
        view.layout(FirstNameField).center(offsetY: -60).left(20).right(20)
    
    }
    
    fileprivate func prepareLastNameField(lastname: String){
        LastNameField = TextField()
        LastNameField.placeholder = "Enter Last Name"
        LastNameField.font = UIFont (name: "Helvetica Neue", size: 20)
        LastNameField.text = lastname
        LastNameField.clearButtonMode = .whileEditing
    
        view.layout(LastNameField).center(offsetY: 0).left(20).right(20)
        
    }
    
    fileprivate func preparePhoneNumberField(phonenumber: String){
        PhoneNumberField = TextField()
        PhoneNumberField.placeholder = "Enter Phone Number"
        PhoneNumberField.font = UIFont (name: "Helvetica Neue", size: 20)
        PhoneNumberField.text = phonenumber
        PhoneNumberField.clearButtonMode = .whileEditing
        PhoneNumberField.keyboardType = UIKeyboardType.phonePad
        
        view.layout(PhoneNumberField).center(offsetY: 60).left(20).right(20)
    }
    
    fileprivate func prepareAddressField(email: String){
        Email = TextField()
        Email.placeholder = "Enter Email"
        Email.font = UIFont (name: "Helvetica Neue", size: 20)
        Email.text = email
        Email.clearButtonMode = .whileEditing
        Email.keyboardType = UIKeyboardType.emailAddress
        
        view.layout(Email).center(offsetY: 120).left(20).right(20)
    }
    
    
    func addEmergencyContact(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string:  baseURL + "api/patient/" + patientIDGlobal + "/contact")!
        
        print(url)
        
        //let parameters = ["username": usernameField.text, "password": passwordField.text]
        let parameters = ["first-name": FirstNameField.text, "last-name": LastNameField.text, "phone-no": PhoneNumberField.text, "email": Email.text]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in

        }
    }
        
    func getEmergencyContactList(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string:  baseURL + "api/patient/" + patientIDGlobal + "/contact/" + emergencycontactIDGlobal)!
        
        print(url)
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response.response?.allHeaderFields)
            print(response)
            switch response.result {
            case .success:
                print("success")
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    
                    self.prepareFirstNameField(firstname: JSON["email"] as! String)
                    self.prepareLastNameField(lastname: JSON["first-name"] as! String)
                    self.preparePhoneNumberField(phonenumber: JSON["last-name"] as! String)
                    self.prepareAddressField(email: JSON["phone-no"] as! String)
                }

                
            case .failure:
                print("Failure to Get Emergency Contact")
            }
        }
    }
    
    func getEmergencyContact(contactID: String){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string:  baseURL + "api/patient/" + patientIDGlobal + "/contact")!
        
        print(url)
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response.response?.allHeaderFields)
            print(response)
            switch response.result {
            case .success:
                print("success")
                
                
            case .failure:
                print("Failure to Get Emergency Contact")
            }
        }
    }
    
    
}




