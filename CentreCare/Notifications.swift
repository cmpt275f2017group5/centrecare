//
//  Notifications.swift
//  CentreCare
//
//  Created by Adnan Syed on 12/3/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import os.log
import Alamofire

var messageIDGlobal: String  = ""

class Notifications: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var tableView: UITableView = UITableView()
    //var patientList = [""]
    var patientList : [String] = []
    var patientIDList : [Int] = []
    var patients = [ProfileInfo]()
    
    var pateintList : [String] = []
    //var testLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getPatientList()
        
        tableView.reloadData()
        
        //self.setNavigationBar()
        //let frame : CGRect = tableView.frame
        //let addButton: UIButton = UIButton(frame: CGRect(x: 0, y: 50, width: self.view.frame.width, height: 100))
        //addButton.setTitle("+", for: .normal)
        //self.view.addSubview(addButton)
        //prepare_testlabel()
        /*
         let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(UIWebView.goBack))
         navigationItem.leftBarButtonItem = backButton
         //self.view.addSubview(backButton)
         let barBtnVar = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(goBack))
         self.navigationItem.setRightBarButton(barBtnVar, animated: true)
         */
        
//        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height:50))
//        //navBar.frame = CGRectMake(0, 0, 320, 50)
//        let navItem = UINavigationItem(title: "Emergency Contacts")
//        let addItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(addButton))
//        navItem.rightBarButtonItem = addItem
//        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
//        navItem.leftBarButtonItem = backButton
//        navBar.setItems([navItem], animated: false)
//        self.view.addSubview(navBar)
        
        
        
        //Constructing tableView.
        let yOffset = CGFloat(100.0)
        self.tableView.frame = CGRect(x:0, y: self.view.frame.origin.y + yOffset, width: self.view.frame.width, height: self.view.frame.height * 0.70);
        
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = 50
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.sectionHeaderHeight = 50
        self.view.addSubview(self.tableView)
    }
    
    
    
    @IBAction func Notifications(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "Notifications") as! Notifications
        self.present(newViewController, animated: true, completion: nil)
    }
    
    @IBAction func ProfileButton(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProfilePage") as! ProfilePage
        self.present(newViewController, animated: true, completion: nil)
    }
    
    @IBAction func SettingsButtons(_ sender: Any) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patientList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
        cell.textLabel?.text = patientList[indexPath.row]
        print(indexPath.row)
        //        var image : UIImage = UIImage(named: "test_patient")!
        //        //print("The loaded image: \(image)")
        //        cell.imageView?.image = image
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Num: \(indexPath.row)")
        print("Value: \(patientList[(indexPath.row)])")
        
        print(patientIDList[indexPath.row])
        messageIDGlobal = String(patientIDList[indexPath.row])
        print(emergencycontactIDGlobal)
        
        //let firstname = "first name test"
        let storyBoard: UIStoryboard = UIStoryboard(name: "EmergencyContact", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddEmergencyContact") as! AddEmergencyContact
        self.present(newViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            // remove the item from the patientList model
            patientList.remove(at: indexPath.row)
            
            // delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            
        } else if editingStyle == .insert {
            tableView.insertRows(at: [indexPath], with: .fade)
            // Not used in our example, but if you were adding a new row, this is where you would do it.
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "You currently have \(patientList.count) Messages"
    }
    
    
    @objc func goBack(){
        //        dismiss(animated: true, completion: nil)
        //        NSLog("back pressed")
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CaretakerPatientMenu") as! CaretakerPatientMenu
        self.present(newViewController, animated: true, completion: nil)
        
    }
    
    /*
     fileprivate func prepare_testlabel() {
     testLabel = UILabel()
     testLabel.text = "Back"
     view.layout(testLabel).center(offsetY: -testLabel.height + 40).left(20).right(20)
     }
     */
    @objc func edit() {
        NSLog("edit pressed")
        patientList.append("new Test Patient \(patientList.count)")
        NSLog("\(patientList[patientList.count-1])")
    }
    @objc func addButton(sender: UIBarButtonItem) {
//        messageIDGlobal = ""
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddEmergencyContact") as! AddEmergencyContact
//        self.present(newViewController, animated: true, completion: nil)
//        //self.present(ViewControllerTest, animated: true, completion: nil)
//        //performSegue(withIdentifier: "segueNameHere", sender: self)
//        //print ("pressed add")
        
    }
    
    
    private func savePatients() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(patients, toFile: ProfileInfo.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Patients successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save patients...", log: OSLog.default, type: .error)
        }
    }
    
    private func loadPatients() -> [ProfileInfo]?  {
        return NSKeyedUnarchiver.unarchiveObject(withFile: ProfileInfo.ArchiveURL.path) as? [ProfileInfo]
    }
    
}

extension Notifications {
    func getPatientList(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string: baseURL + "api/caretaker/" + patientIDGlobal + "/messages")!
        
        print(url)
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response)
            switch response.result {
            case .success:
                
                //Transfering the responce to JSON and assigning the value
                
                if let result = response.result.value as? NSArray{
                    print(type(of: result))
                    
                    for item in result { // loop through patientList items
                        print(item)
                        
                        self.patientList.append("new Test Patient \(item)")
                        self.patientIDList.append(item as! Int)
                        //                        let newIndexPath = IndexPath(row: self.patientList.count, section: 0)
                        //                        self.tableView.insertRows(at: [newIndexPath], with: .automatic)
                    }
                    self.tableView.reloadData()
                }
                
            case .failure:
                print("failed getting patientlist")
                //                // create the alert
                //                let alert = UIAlertController(title: "Login Error", message: "Invalid username or password", preferredStyle: UIAlertControllerStyle.alert)
                //
                //                // add an action (button)
                //                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                //
                //                // show the alert
                //                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getNotification(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string: baseURL + "api/caretaker/" + patientIDGlobal + "/messages/0")!
        
        print(url)
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                
                //Transfering the responce to JSON and assigning the value
                print("Success")
    //                if let result = response.result.value {
    //                    //let JSON = result as! NSDictionary
    //                    //Set access token to sessionID which is a global set in appdelegate.swift
    //                   
    //                }
                
            case .failure:
                print("Failed")
//                // create the alert
//                let alert = UIAlertController(title: "Login Error", message: "Invalid username or password", preferredStyle: UIAlertControllerStyle.alert)
//
//                // add an action (button)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//
//                // show the alert
//                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

