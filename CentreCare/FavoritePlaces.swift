//
//  FavoritePlaces.swift
//  CentreCare
//
//  Created by Swimm Chan on 2017-11-27.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material

var constant: CGFloat = -150
class FavoritePlaces: UIViewController {
    
    var tempArray:[String] = []

    override func viewDidLoad() {
        
        //var index = 0
        //var constant = 0
        super.viewDidLoad()
        
        if(AddressArray.count != 0){
            tempArray = AddressArray
            FavPlaceButton()
        }
        if(AddressArray.count > 2){
            constant = -125
            tempArray[0] = AddressArray[2]
            FavPlaceButton()
        }
        if(AddressArray.count > 4){
            constant = -100
            tempArray[0] = AddressArray[4]
            FavPlaceButton()
        }

        
    }
    
    func goToFavLocationPage(sender: UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "FavoriteLocationProfile") as! FavoriteLocationProfile
        self.present(newViewController, animated: true, completion: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension FavoritePlaces{
    
    fileprivate func FavPlaceButton(){
        let favButton = FlatButton(title: tempArray[0], titleColor: .black)
        favButton.pulseColor = .black
        favButton.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 25)
        favButton.addTarget(self, action: #selector(goToFavLocationPage), for: .touchUpInside)
        
        view.layout(favButton).width(300).height(50).center(offsetY:constant).right(120)
    }
    
    
}
