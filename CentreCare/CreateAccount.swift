//
//  CreateAccount.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/11/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material
import SwiftHEXColors
import Alamofire



//The makes the user set a username and password of the account. This is saved on the backend and can be used to login
class CreateAccount: UIViewController, UITextFieldDelegate {
    
    fileprivate var usernameField: TextField!
    fileprivate var passwordField: TextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the background color of the page
        view.backgroundColor = UIColor(hex: 0xa8c3e2)
        
        prepareUsernameField()
        preparePasswordField()
        prepareLogoImage()
        prepareCreateAccountButton()
        
        //Looks for tap on the screen.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CreateAccount.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    //If they select signUp, they go to the the account is created and they move to the profile page, to create a profile
    internal func signUp(button: UIButton) {
        
        //Saves the credentials on the server
        if(userType == "caretaker" || userType == "CARETAKER"){
            createAccountAlomafire(username: usernameField.text!, password: passwordField.text!)
            
            sleep(1)
            
            login(username: usernameField.text!, password: passwordField.text!)
        
        }
        else{
            createPatientAccountAlomafire(username: usernameField.text!, password: passwordField.text!)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension CreateAccount {
    
    //Dismisses the keyboard
    func dismissKeyboard() {
        view.endEditing(true)
    }
    // Sets the Username field on viewcontroller
    fileprivate func prepareUsernameField() {
        usernameField = TextField()
        usernameField.placeholder = "Username"
        usernameField.detail = "At least 5 characters"
        usernameField.font = UIFont (name: "Helvetica Neue", size: 25)
        usernameField.clearButtonMode = .whileEditing

        view.layout(usernameField).center(offsetY: -50).left(20).right(20)
    }
    // Sets the password field on viewcontroller
    fileprivate func preparePasswordField() {
        passwordField = TextField()
        passwordField.placeholder = "Password"
        passwordField.detail = "At least 8 characters"
        passwordField.font = UIFont (name: "Helvetica Neue", size: 25)
        passwordField.clearButtonMode = .whileEditing
        passwordField.isVisibilityIconButtonEnabled = true
 
        view.layout(passwordField).center(offsetY: 50).left(20).right(20)
    }
    
    //Sets the properties of the logo on the page
     func prepareLogoImage(){
        let imageName = "appicon"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        
        imageView.center = self.view.center
        imageView.frame = CGRect(x: ((UIScreen.main.bounds.width) * 0.5) - 50, y: (UIScreen.main.bounds.height) * 0.1 , width: 100, height: 100)
        view.addSubview(imageView)
    }
    
    //Sets the properties of the create Account on the page
    fileprivate func prepareCreateAccountButton() {
        let createAccountButton = RaisedButton(title: "Create Account", titleColor: .white)
        createAccountButton.pulseColor = .white
        createAccountButton.backgroundColor = Color.blue.lighten1
        createAccountButton.addTarget(self, action: #selector(signUp), for: .touchUpInside)
        
        view.layout(createAccountButton)
            .width(ButtonLayout.loginButton.width)
            .height(ButtonLayout.loginButton.height)
            .center(offsetY: ButtonLayout.loginButton.offsetY)
    }
    
    //Login function query from database
    func login(username: String, password: String){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let url = URL(string:  baseURL + "api/session")!
        
        let parameters = ["username": username, "password": password]
        //let parameters = ["username": "pairtest1", "password": "secret"]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            //print(response.response?.allHeaderFields)
            if let xID = response.response?.allHeaderFields["X-PID"] as? String {
                // use contentType here
                userID = xID
            }
            
            if let xID = response.response?.allHeaderFields["X-CID"] as? String {
                // use contentType here
                userID = xID
            }

            switch response.result {
            case .success:
                
                //Transfering the responce to JSON and assigning the value
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    //Set access token to sessionID which is a global set in appdelegate.swift
                    sessionId = JSON["access-token"] as! String
                    
                    print(JSON)
                    
                    //Goes to the profile page view
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProfilePage") as! ProfilePage
                    self.present(newViewController, animated: true, completion: nil)

   
                }
                
            case .failure:
                // create the alert
                let alert = UIAlertController(title: "Login Error AlamoFire", message: "Invalid username or password Alamo", preferredStyle: UIAlertControllerStyle.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    
    //This take in the username and password and paratmeterts and does a post request to the server.
    //The server saves the login credentials
    func createAccountAlomafire(username: String, password: String){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let url = URL(string:  baseURL + "api/" + userType)!
        
        let parameters = ["username": username, "password": password]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response.response?.allHeaderFields)
            if let xID = response.response?.allHeaderFields["X-PID"] as? String {
                // use contentType here
                userID = xID
            }
            
            if let xID = response.response?.allHeaderFields["X-CID"] as? String {
                // use contentType here
                userID = xID
            }
            
            switch response.result {
            case .success:
                
                //Transfering the responce to JSON and assigning the value
                if let result = response.result.value {
                    let JSON = result as! NSDictionary

                }
                
                print("Success")
                
            case .failure:
                print("failed")
            }
        }
    }
    
    //Function when selected create patient
    func createPatientAccountAlomafire(username: String, password: String){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Pairing-Code" : pairingCode
        ]
        let url = URL(string:  baseURL + "api/" + userType)!
        
        let parameters = ["username": username, "password": password]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response.response?.allHeaderFields)
            if let xID = response.response?.allHeaderFields["X-PID"] as? String {
                // use contentType here
                userID = xID
            }
            
            if let xID = response.response?.allHeaderFields["X-CID"] as? String {
                // use contentType here
                userID = xID
            }
            
            
            //Goes to the profile page view
            let storyBoard: UIStoryboard = UIStoryboard(name: "PatientScreen", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "PatientMenu") as! PatientMenu
            self.present(newViewController, animated: true, completion: nil)

        }
    }
    
    
}

