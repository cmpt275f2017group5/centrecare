//
//  QRCodeGeneratorPage.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/13/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Alamofire

class QRCodeGeneratorPage: UIViewController {

    @IBOutlet weak var qrCodeImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getPairingCode()

        
    }
    //Navigation buttons
    @IBAction func backButtonAction(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "PatientList", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PatientList") as! PatientList
        self.present(newViewController, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension QRCodeGeneratorPage{
    //Retrieve a QR code from the database
    func generateQRCode(from string: String) -> UIImage? {
        
        let data = string.data(using: String.Encoding.isoLatin1)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator"){
            filter.setValue(data, forKey: "inputMessage")
            
            guard let qrCodeImage = filter.outputImage else { return nil}
            
            let scaleX = qrCodeImageView.frame.size.width / qrCodeImage.extent.size.width
            let scaleY = qrCodeImageView.frame.size.height / qrCodeImage.extent.size.height
            
            let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
            
            if let output = filter.outputImage?.applying(transform){
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    //Get the pairing from the database
    func getPairingCode(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]

        let url = URL(string: "http://centrecare.org/api/caretaker/" + userID + "/pairing")!

        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in

            print(response.response?.allHeaderFields["X-Pairing-Code"] as! String!)
            
            let image = self.generateQRCode(from: response.response?.allHeaderFields["X-Pairing-Code"] as! String!)
            self.qrCodeImageView.image = image

        }
    }


}
