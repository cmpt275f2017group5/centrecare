//
//  PatientProfileView.swift
//  CentreCare
//
//  Created by Jason Trinh on 2017-11-05.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit

class PatientProfileView: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        first_name.delegate = self
        last_name.delegate = self
    }
   
    @IBOutlet weak var first_name: UITextField!
    @IBOutlet weak var last_name: UITextField!
    
    @IBOutlet weak var invalid_input_label: UILabel!
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    //Check if the user is done editing the text
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.text?.isEmpty == true){
            textField.attributedPlaceholder = NSAttributedString(string: "Required Input", attributes: [NSForegroundColorAttributeName: UIColor.red])
        }
    }
    
    //Check if the charecters are valid in the text field
    func textField(_ textFieldToChange: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var characterSetNotAllowed = CharacterSet.punctuationCharacters
        characterSetNotAllowed.formUnion(CharacterSet.decimalDigits)
        
        characterSetNotAllowed.formUnion(CharacterSet.symbols)
        
  
        if let _ = string.rangeOfCharacter(from: characterSetNotAllowed, options: .caseInsensitive) {
            invalid_input_label.text = "Invalid format, only accepts alphabet characters ex: \"John\""
            return false
        } else {
            invalid_input_label.text = ""
            return true
        }
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

