//
//  AccountType.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/15/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material
import SwiftHEXColors

//This is the page where the user selects which type account he/she wants to create
class AccountType: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the background color of the page
        view.backgroundColor = UIColor(hex: 0xa8c3e2)
        
        //Displays the buttons
        prepareCaretakerButton()
        preaparePatientButton()
        prepareLogoImage()

    }
    
    
    //If user choose the caretaker, it will save the account type it selected and move on to the create account page
    internal func caretakerAccountSelect(button: UIButton) {
        userType = "caretaker"
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CreateAccount") as! CreateAccount
        self.present(newViewController, animated: true, completion: nil)
    }
    
    //If the user chooses the patient, it will save the account type the user selected and moves on to the create account page
    func patientAccountSelect(sender: UIButton)
    {
        userType = "patient"
        
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            let storyBoard: UIStoryboard = UIStoryboard(name: "PatientList", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "SimulatorQRCodeReader") as! SimulatorQRCodeReader
            self.present(newViewController, animated: true, completion: nil)
        #else
            let storyBoard: UIStoryboard = UIStoryboard(name: "PatientScreen", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "QRCodeReaderPage") as! QRCodeReaderPage
            self.present(newViewController, animated: true, completion: nil)
        #endif
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension AccountType {
    
    //Dismisses the keyboard
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //Sets the logo image properties
    func prepareLogoImage(){
        let imageName = "appicon"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        
        imageView.center = self.view.center
        imageView.frame = CGRect(x: ((UIScreen.main.bounds.width) * 0.5) - 100, y: (UIScreen.main.bounds.height) * 0.09 , width: 200, height: 200)
        view.addSubview(imageView)
    }
    
    //Sets the caretaker button properties
    fileprivate func prepareCaretakerButton() {
        let caretakerButton = RaisedButton(title: "Caretaker", titleColor: .white)
        caretakerButton.pulseColor = .white
        caretakerButton.backgroundColor = Color.blue.lighten1
        caretakerButton.addTarget(self, action: #selector(caretakerAccountSelect), for: .touchUpInside)
        
        view.layout(caretakerButton)
            .width(ButtonLayout.caretakerButton.width)
            .height(ButtonLayout.caretakerButton.height)
            .center(offsetX: ButtonLayout.caretakerButton.offsetX)
    }
    
    //Sets the patient button properties
    fileprivate func preaparePatientButton(){
        let patientButton = RaisedButton(title: "Patient", titleColor: .white)
        patientButton.pulseColor = .white
        patientButton.backgroundColor = Color.purple.lighten1
        patientButton.addTarget(self, action: #selector(patientAccountSelect), for: .touchUpInside)
        
        view.layout(patientButton)
            .width(ButtonLayout.patientButton.width)
            .height(ButtonLayout.patientButton.height)
            .center(offsetX: ButtonLayout.patientButton.offsetX)
        
    }
}
