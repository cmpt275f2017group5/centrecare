//
//  SimulatorQRCodeReader.swift
//  CentreCare
//
//  Created by Adnan Syed on 12/3/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Alamofire

class SimulatorQRCodeReader: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkLoginAlomafire()

        // Do any additional setup after loading the view.
    }
    @IBAction func Continue(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CreateAccount") as! CreateAccount
        self.present(newViewController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension SimulatorQRCodeReader {
    
    func checkLoginAlomafire(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        let url = URL(string: baseURL + "api/session")!
        
        print(url)
        
        //let parameters = ["username": usernameField.text, "password": passwordField.text]
        let parameters = ["username": "asdfasdf", "password": "secret"]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            var localUserID : String = ""
            //print(response.response?.allHeaderFields)
            if let xID = response.response?.allHeaderFields["X-PID"] as? String {
                // use contentType here
                localUserID = xID
            }
            
            if let xID = response.response?.allHeaderFields["X-CID"] as? String {
                // use contentType here
                localUserID = xID
            }
            switch response.result {
            case .success:
                
                //Transfering the responce to JSON and assigning the value
                
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    
                    self.getPairingCode(localSessionID: JSON["access-token"] as! String, localUserID: localUserID)
                }
                
            case .failure:
                // create the alert
                let alert = UIAlertController(title: "Login Error", message: "Invalid username or password", preferredStyle: UIAlertControllerStyle.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    // Get Pairing code from the database
    func getPairingCode(localSessionID: String, localUserID: String){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": localSessionID
        ]
        
        let url = URL(string: "http://centrecare.org/api/caretaker/" + localUserID + "/pairing")!
        
        print(url)
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response.response?.allHeaderFields["X-Pairing-Code"] as! String!)
            
            pairingCode = response.response?.allHeaderFields["X-Pairing-Code"] as! String!
        }
    }
}
