//
//  PatientPairing.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/6/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit

class PatientPairing: UIViewController {

    @IBOutlet weak var accessCodeInput: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Take them directly to the QR generating page
        let storyBoard: UIStoryboard = UIStoryboard(name: "PatientScreen", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "QRCodeGeneratorPage") as! QRCodeGeneratorPage
        self.present(newViewController, animated: true, completion: nil)

        
        // Do any additional setup after loading the view.
    }

    @IBAction func submitAction(_ sender: Any) {
        
//        // create the alert
//        let alert = UIAlertController(title: "Unavailable", message: "Feature Disabled", preferredStyle: UIAlertControllerStyle.alert)
//        
//        // add an action (button)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//        
//        // show the alert
//        self.present(alert, animated: true, completion: nil)
        
        //return
        let storyBoard: UIStoryboard = UIStoryboard(name: "PatientScreen", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "QRCodeReaderPage") as! QRCodeReaderPage
        self.present(newViewController, animated: true, completion: nil)

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
