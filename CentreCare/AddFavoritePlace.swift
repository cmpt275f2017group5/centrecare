//
//  AddFavoritePlace.swift
//  CentreCare
//
//  Created by Swimm Chan on 2017-11-27.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material
import Alamofire

var AddressArray:[String] = []

class AddFavoritePlace: UIViewController {

    fileprivate var placeNameField: TextField!
    fileprivate var addressField: TextField!
    
    fileprivate var streetField:TextField!
    fileprivate var postalcodeField:TextField!
    fileprivate var cityField:TextField!
    fileprivate var provinceField:TextField!
    fileprivate var countryField:TextField!
    
    fileprivate var name : String = "";
    fileprivate var street : String = "";
    fileprivate var city : String = "";
    fileprivate var postalcode : String = "";
    fileprivate var country : String = "";
    fileprivate var province : String = "";

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(favoritelocationIDGlobal == ""){
            preparePlaceNameField(name: name)
            preparestreetField(street: street)
            preparecityField(city: city)
            preparepostalcodeField(postalcode: postalcode)
            preparecountryField(country: country)
            prepareprovinceField(province: province)
        }
        else{
//            getFavouritePlace()
        }
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UniversalLogin.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SaveButton(_ sender: Any) {
        addFavouritePlace()
        //AddressArray.append(placeNameField.text!)
        //AddressArray.append(addressField.text!)
        
    }
    
    
}


extension AddFavoritePlace{
    
    //This will close the keyboard
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    fileprivate func preparePlaceNameField(name: String){
        placeNameField = TextField()
        placeNameField.placeholder = "Nickname for location"
        placeNameField.text = name
        placeNameField.clearButtonMode = .whileEditing
        
        view.layout(placeNameField).center(offsetY: -placeNameField.height - 100).left(20).right(20)
    }
    
    fileprivate func preparestreetField(street: String) {
        streetField = ErrorTextField()
        streetField.placeholder = "Street"
        streetField.detail = "Error: "
        streetField.isClearIconButtonEnabled = true
        
        streetField.text = street
        
       
        
        streetField.keyboardType = UIKeyboardType.default
        view.layout(streetField).center(offsetY: -streetField.height - 50).left(20).right(20)
    }
    
    fileprivate func preparepostalcodeField(postalcode: String) {
        postalcodeField = TextField()
        postalcodeField.placeholder = "Postal Code"
        postalcodeField.isClearIconButtonEnabled = true
        
        postalcodeField.text = postalcode
        
       
        
        postalcodeField.keyboardType = UIKeyboardType.default
        view.layout(postalcodeField).center(offsetY: -postalcodeField.height + 0).left(20).width(150)
    }
    
    fileprivate func preparecityField(city: String) {
        cityField = TextField()
        cityField.placeholder = "City"
        cityField.isClearIconButtonEnabled = true
        
        cityField.text = city
        
       
        
        cityField.keyboardType = UIKeyboardType.default
        view.layout(cityField).center(offsetY: -cityField.height + 0).right(20).width(180)
    }
    fileprivate func prepareprovinceField(province: String) {
        provinceField = TextField()
        provinceField.placeholder = "Province"
        provinceField.isClearIconButtonEnabled = true
        
        provinceField.text = province
        
       
        
        provinceField.keyboardType = UIKeyboardType.default
        view.layout(provinceField).center(offsetY: -provinceField.height + 50).left(20).width(150)
    }
    fileprivate func preparecountryField(country: String) {
        countryField = TextField()
        countryField.placeholder = "Country"
        countryField.isClearIconButtonEnabled = true
        
        countryField.text = country

        countryField.keyboardType = UIKeyboardType.default
        view.layout(countryField).center(offsetY: -countryField.height + 50).right(20).width(180)
    }
    
    func addFavouritePlace(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        //userType = "patient"
        
        let parameters : [String: Any] = [
            "name": placeNameField.text,
            "address": [
                "street": streetField.text,
                "city": cityField.text,
                "province": provinceField.text,
                "country": countryField.text,
                "postal-code": postalcodeField.text
            ]
        ]
        
        let url = URL(string:  baseURL + "api/patient/" + patientIDGlobal + "/favourite")!
        Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            //Sends the put request
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "FavoritePlacesList") as! FavoritePlacesList
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    func getFavouritePlace(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        if(userType == "CARETAKER"){
            userType = "caretaker"
        }
        
        let url = URL(string:  baseURL + "api/" + "patient" + "/" + patientIDGlobal + "/favourite" + favoritelocationIDGlobal)!
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response.response?.allHeaderFields)
            print(response)
            
            switch response.result {
            case .success:
                
                //Transfering the responce to JSON and assigning the value
                
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    let address = JSON["address"] as! NSDictionary
                    
                    //self.email = JSON["email"] as! String!
                    self.name = JSON["name"] as! String!
                    self.city = address["city"] as! String!
                    self.country = address["country"] as! String!
                    self.postalcode = address["postal-code"] as! String!
                    self.province = address["province"] as! String!
                    self.street = address["street"] as! String!
                    
                    
                    //self.prepareEmailField(email: self.email)
                    self.preparePlaceNameField(name: self.name)
                    self.preparestreetField(street: self.street)
                    self.preparecityField(city: self.city)
                    self.preparepostalcodeField(postalcode: self.postalcode)
                    self.preparecountryField(country: self.country)
                    self.prepareprovinceField(province: self.province)
                    
                    
                }
                
            case .failure:
                // create the alert
                let alert = UIAlertController(title: "Login Error", message: "Invalid username or password", preferredStyle: UIAlertControllerStyle.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
}
