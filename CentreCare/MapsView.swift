//
//  MapsView.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/5/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Mapbox
import CoreLocation
import Foundation
import Material
import Alamofire

//This pages allows to person to see the current geofence size and modify it
//The geofence setting currently doesnt save
class MapsView: UIViewController, MGLMapViewDelegate {
    
    var mapView: MGLMapView!
    var polygon : MGLPolygon? = nil
    var sliderChanged : Bool = false
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var radiusLabel: UILabel!
    
    
    let setGeofenceButton = RaisedButton(title: "Set Geofence", titleColor: .white)
    let editButton = RaisedButton(title: "Edit", titleColor: .white)
    
    let center = CLLocationCoordinate2D(latitude: 49.276765, longitude: -122.917957)
    var radiusMeters : Double = 500.00
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareEditButton()
        prepareSetGeofenceButton()
        getGeofence()
        
        //getLastKnownLocationAlamofire()
        
        //getGeofence()
        
        slider.isHidden = true
        setGeofenceButton.isHidden = true
        radiusLabel.text = String(Int(radiusMeters))
        
        radiusLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
        
        let screenSize = UIScreen.main.bounds
        let rect = CGRect(x: 0, y: 100, width: screenSize.width, height: (0.67 * screenSize.height))
        
        //Set the map type and assign the map size to map
        let url = URL(string: "mapbox://styles/mapbox/streets-v10")
        mapView = MGLMapView(frame: rect, styleURL: url)
        
        //mapView = MGLMapView(frame: view.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        mapView.setCenter(center, zoomLevel: 14, animated: false)
        
        mapView.delegate = self
        view.addSubview(mapView)

    }
    
    //When the edit button is pressed, the option to change the geofence shows
    internal func editGeofence(button: UIButton) {
        if(slider.isHidden == true ){
            editButton.backgroundColor = Color.red.lighten1
            slider.isHidden = false;
            setGeofenceButton.isHidden = false;
        }
        else{
            editButton.backgroundColor = Color.blue.lighten1
            slider.isHidden = true;
            setGeofenceButton.isHidden = true;
        }
        
    }
    
    //When the use sets the geofence, it return them back to the caretaker menu page
    internal func setGeofence(sender: UIButton){
        saveGeofence(latitude: center.latitude, longitude: center.longitude, radius: radiusMeters)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CaretakerPatientMenu") as! CaretakerPatientMenu
        self.present(newViewController, animated: true, completion: nil)
    }
    
    //Changes the level of the zoom on the map view depending on the size of the radius is set
    @IBAction func zoomSlider(_ sender: UISlider) {
        print(Int(sender.value))
        
        radiusMeters = Double(sender.value)
        
        radiusLabel.text = String(Int(sender.value))
        
        sliderChanged = true
        
        let sliderValue : Int = Int(sender.value)
        
        if(sliderValue < 100){
            mapView.setCenter(center, zoomLevel: 14, animated: false)
            mapView.delegate = self
            view.addSubview(mapView)
        }
        else if(sliderValue < 500){
            mapView.setCenter(center, zoomLevel: 13, animated: false)
            mapView.delegate = self
            view.addSubview(mapView)
        }
        else if(sliderValue < 1000){
            mapView.setCenter(center, zoomLevel: 12, animated: false)
            mapView.delegate = self
            view.addSubview(mapView)
        }
        else if(sliderValue < 5000){
            mapView.setCenter(center, zoomLevel: 11, animated: false)
            mapView.delegate = self
            view.addSubview(mapView)
        }
        else if(sliderValue < 10000){
            mapView.setCenter(center, zoomLevel: 10, animated: false)
            mapView.delegate = self
            view.addSubview(mapView)
        }
        else{
            mapView.setCenter(center, zoomLevel: 9, animated: false)
            mapView.delegate = self
            view.addSubview(mapView)
        }

        polygonCircleForCoordinate(coordinate: center, withMeterRadius: radiusMeters)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        polygonCircleForCoordinate(coordinate: center, withMeterRadius: radiusMeters)
    }
    
    
}

extension MapsView {
    
    //Generates the coordinates to draw the circle polygon when given a radius
    func polygonCircleForCoordinate(coordinate: CLLocationCoordinate2D, withMeterRadius: Double) {
        let degreesBetweenPoints = 8.0
        //45 sides
        let numberOfPoints = floor(360.0 / degreesBetweenPoints)
        let distRadians: Double = withMeterRadius / 6371000.0
        // earth radius in meters
        let centerLatRadians: Double = coordinate.latitude * Double.pi / 180
        let centerLonRadians: Double = coordinate.longitude * Double.pi / 180
        var coordinates = [CLLocationCoordinate2D]()
        //array to hold all the points
        for index in 0 ..< Int(numberOfPoints) {
            let degrees: Double = Double(index) * Double(degreesBetweenPoints)
            let degreeRadians: Double = degrees * Double.pi / 180
            let pointLatRadians: Double = asin(sin(centerLatRadians) * cos(distRadians) + cos(centerLatRadians) * sin(distRadians) * cos(degreeRadians))
            let pointLonRadians: Double = centerLonRadians + atan2(sin(degreeRadians) * sin(distRadians) * cos(centerLatRadians), cos(distRadians) - sin(centerLatRadians) * sin(pointLatRadians))
            let pointLat: Double = pointLatRadians * 180 / Double.pi
            let pointLon: Double = pointLonRadians * 180 / Double.pi
            let point: CLLocationCoordinate2D = CLLocationCoordinate2DMake(pointLat, pointLon)
            coordinates.append(point)
        }
        if(polygon != nil){
            mapView.remove(polygon!)
        }
        
        polygon = MGLPolygon(coordinates: &coordinates, count: UInt(coordinates.count))
        mapView.add(polygon!)
    }
    
    //Sets the opacity of the polygon
    func mapView(_ mapView: MGLMapView, alphaForShapeAnnotation annotation: MGLShape) -> CGFloat {
        return 0.5
    }
    
    //Sets the color of the polygon
    func mapView(_ mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        return .purple
    }
    
    //Set the fill color of the polygon
    func mapView(_ mapView: MGLMapView, fillColorForPolygonAnnotation annotation: MGLPolygon) -> UIColor {
        return .purple
    }
    
    //Sets the properties of the edit button
    fileprivate func prepareEditButton() {
        editButton.pulseColor = .white
        editButton.backgroundColor = Color.blue.lighten1
        editButton.addTarget(self, action: #selector(editGeofence), for: .touchUpInside)
        
        view.layout(editButton)
            .width(ButtonLayout.editButton.width)
            .height(ButtonLayout.editButton.height)
            .left(ButtonLayout.editButton.offsetX)
            .center(offsetY: ButtonLayout.editButton.offsetY)
        
    }
    
    //Set the properties of the set geofence button
    fileprivate func prepareSetGeofenceButton(){
        setGeofenceButton.pulseColor = .white
        setGeofenceButton.backgroundColor = Color.purple.lighten1
        setGeofenceButton.addTarget(self, action: #selector(setGeofence), for: .touchUpInside)
        
        view.layout(setGeofenceButton)
            .width(ButtonLayout.setGeofenceButton.width)
            .height(ButtonLayout.setGeofenceButton.height)
            .center(offsetY: ButtonLayout.setGeofenceButton.offsetY)
        
    }
    
//    //Gets the last kwown location of the patient
//    func getLastKnownLocation(){
//
//        let httpGetUrl = URL(string:  baseURL + "api/patient/12/location")
//
//        //create the session object
//        let session = URLSession.shared
//
//        var request = URLRequest(url: httpGetUrl!)
//
//        request.httpMethod = "GET"
//        request.setValue(sessionId, forHTTPHeaderField: "X-Access-Token")
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//
//        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
//
//            guard let data = data else {
//                return
//            }
//
//            do {
//                //create json object from data
//                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
//                    print(json)
//
//                    //Set the information on the map
//
//                    //Go to coordination on map
//                    self.mapView.setCenter(CLLocationCoordinate2D(latitude: (json["latitude"] as! CLLocationDegrees), longitude: (json["longitude"] as! CLLocationDegrees)), zoomLevel: 14, animated: true)
//
//                    // Set the last known location of the patient on the map
//                    let annotation = MGLPointAnnotation()
//
//                    annotation.coordinate = CLLocationCoordinate2D(latitude: (json["latitude"] as! CLLocationDegrees), longitude: (json["longitude"] as! CLLocationDegrees))
//
//                    annotation.title = "Patient Location"
//                    annotation.subtitle = "Last Seen"
//
//                    self.mapView.addAnnotation(annotation)
//
//                    self.mapView.delegate = self
//
//                }
//            } catch let error {
//                print(error.localizedDescription)
//            }
//
//        })
//        task.resume()
//    }
    
    func getLastKnownLocationAlamofire(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string:  baseURL + "patient/" + patientIDGlobal + "/location")!
        
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
          
            print(response)
            
            switch response.result {
            case .success:
                print("success")
                //Transfering the responce to JSON and assigning the value
                
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    
                    //print(result)
//                    self.radiusMeters = Double(JSON["radius"] as! Double)
//
//                    self.radiusLabel.text = String(Int(JSON["radius"] as! Double))
//
//                    let sliderValue : Int = Int(JSON["radius"] as! Double)
//
//                    if(sliderValue < 100){
//                        self.mapView.setCenter(self.center, zoomLevel: 14, animated: false)
//                        self.mapView.delegate = self
//                        self.view.addSubview(self.mapView)
//                    }
//                    else if(sliderValue < 500){
//                        self.mapView.setCenter(self.center, zoomLevel: 13, animated: false)
//                        self.mapView.delegate = self
//                        self.view.addSubview(self.mapView)
//                    }
//                    else if(sliderValue < 1000){
//                        self.mapView.setCenter(self.center, zoomLevel: 12, animated: false)
//                        self.mapView.delegate = self
//                        self.view.addSubview(self.mapView)
//                    }
//                    else if(sliderValue < 5000){
//                        self.mapView.setCenter(self.center, zoomLevel: 11, animated: false)
//                        self.mapView.delegate = self
//                        self.view.addSubview(self.mapView)
//                    }
//                    else if(sliderValue < 10000){
//                        self.mapView.setCenter(self.center, zoomLevel: 10, animated: false)
//                        self.mapView.delegate = self
//                        self.view.addSubview(self.mapView)
//                    }
//                    else{
//                        self.mapView.setCenter(self.center, zoomLevel: 9, animated: false)
//                        self.mapView.delegate = self
//                        self.view.addSubview(self.mapView)
//                    }
//
//                    self.polygonCircleForCoordinate(coordinate: self.center, withMeterRadius: self.radiusMeters)
                    
                }
                
            case .failure:
                print("Failure to get Location")
//                // create the alert
//                let alert = UIAlertController(title: "Login Error", message: "Invalid username or password", preferredStyle: UIAlertControllerStyle.alert)
//
//                // add an action (button)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//
//                // show the alert
//                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func saveGeofence(latitude: Double, longitude: Double, radius: Double){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string:  baseURL + "api/patient/" + patientIDGlobal + "/geofence")!
        
        let parameters = ["latitude": latitude, "longitude": longitude, "radius": radius]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            //This should be working!!!
            
            print(response)
            
            switch response.result {
            case .success:
                print("success")
                //Transfering the responce to JSON and assigning the value
                
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                }
                
                
                
            case .failure:
                print("Save Geofence Failer")
//                // create the alert
//                let alert = UIAlertController(title: "Login Error", message: "Invalid username or password", preferredStyle: UIAlertControllerStyle.alert)
//
//                // add an action (button)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//
//                // show the alert
//                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getGeofence(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string:  baseURL + "api/patient/" + patientIDGlobal + "/geofence/last")!
        
        //let parameters = ["latitude": latitude, "longitude": longitude, "radius": radius]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                print("success")
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    //print(JSON["radius"])
                    
                    self.radiusMeters = Double(JSON["radius"] as! Double)

                    self.radiusLabel.text = String(Int(JSON["radius"] as! Double))

                    let sliderValue : Int = Int(JSON["radius"] as! Double)

                    if(sliderValue < 100){
                        self.mapView.setCenter(self.center, zoomLevel: 14, animated: false)
                        self.mapView.delegate = self
                        self.view.addSubview(self.mapView)
                    }
                    else if(sliderValue < 500){
                        self.mapView.setCenter(self.center, zoomLevel: 13, animated: false)
                        self.mapView.delegate = self
                        self.view.addSubview(self.mapView)
                    }
                    else if(sliderValue < 1000){
                        self.mapView.setCenter(self.center, zoomLevel: 12, animated: false)
                        self.mapView.delegate = self
                        self.view.addSubview(self.mapView)
                    }
                    else if(sliderValue < 5000){
                        self.mapView.setCenter(self.center, zoomLevel: 11, animated: false)
                        self.mapView.delegate = self
                        self.view.addSubview(self.mapView)
                    }
                    else if(sliderValue < 10000){
                        self.mapView.setCenter(self.center, zoomLevel: 10, animated: false)
                        self.mapView.delegate = self
                        self.view.addSubview(self.mapView)
                    }
                    else{
                        self.mapView.setCenter(self.center, zoomLevel: 9, animated: false)
                        self.mapView.delegate = self
                        self.view.addSubview(self.mapView)
                    }

                    self.polygonCircleForCoordinate(coordinate: self.center, withMeterRadius: self.radiusMeters)
                    
                    
                }
                //Transfering the responce to JSON and assigning the value
                //
                
                
            case .failure:
                print("Get Geofence Failure")

            }
        }
    }



}
