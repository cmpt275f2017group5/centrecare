//
//  EmergencyContact.swift
//  CentreCare
//
//  Created by Swimm Chan on 2017-11-21.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material
import Alamofire

var contactCreated : Bool = false

class EmergencyContact: UIViewController {

    //var passedInfo: [String] = []
    //var currentInfo: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getEmergencyContactList()
        /*
        if (SecondEC.count == 0){
            passedInfo = EntryArray
        }
        else{
            passedInfo = SecondEC
        }
        
        currentInfo = passedInfo
        */
        //var DisplayName = UILabel()
        
        //var FirstNameField: TextField!
        
        //var DisplayButon = UIButton()

        //FirstNameField = TextField()
//        if (EntryArray.count != 0){
//            emergencyContactButton()
//        }
        if (SecondEC.count != 0){
            SecondECButton()
        }
        if (ThirdEC.count != 0){
            ThirdECButton()
        }
        
        // Do any additional setup after loading the view.
    }
    
    func goToProfilePage(sender: UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "EmergencyContact", bundle:nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "AddEmergencyContact") as! AddEmergencyContact
        self.present(newViewController, animated: true, completion: nil)
    }
    
    func goToSecondProfilePage(sender: UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "EmergencyContact", bundle:nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "SecondECProfile") as! SecondECProfile
        self.present(newViewController, animated: true, completion: nil)
    }
    
    func goToThirdProfilePage(sender: UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "EmergencyContact", bundle:nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "ThirdECProfile") as! ThirdECProfile
        self.present(newViewController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension EmergencyContact {
    
    fileprivate func emergencyContactButton(name: String){
        let ecButton = FlatButton(title: name, titleColor: .black)
        ecButton.pulseColor = .black
        ecButton.titleLabel?.font = UIFont(name: "Helvetica Neue", size:25)
        //ecButton.text = name
        //ecButton.backgroundColor = Color.purple.lighten1
        ecButton.addTarget(self, action: #selector(goToProfilePage), for: .touchUpInside)
        
        
        view.layout(ecButton)
            .width(ButtonLayout.emergencyContactProfileButton.width)
            .height(ButtonLayout.emergencyContactProfileButton.height)
            .center(offsetY: ButtonLayout.emergencyContactProfileButton.offsetY).right(120)
    }
    
    fileprivate func SecondECButton(){
        let secondECButton = FlatButton(title: SecondEC[0], titleColor: .black)
        secondECButton.pulseColor = .black
        secondECButton.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 25)
        secondECButton.addTarget(self, action: #selector(goToSecondProfilePage), for: .touchUpInside)
        
        view.layout(secondECButton).width(300).height(50).center(offsetY: -80).right(120)
    }
    
    fileprivate func ThirdECButton(){
        let thirdECButton = FlatButton(title: ThirdEC[0], titleColor: .black)
        thirdECButton.pulseColor = .black
        thirdECButton.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 25)
        thirdECButton.addTarget(self, action: #selector(goToThirdProfilePage), for: .touchUpInside)
        
        view.layout(thirdECButton).width(300).height(50).center(offsetY: -20).right(120)
    }
    
    func getEmergencyContactList(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        let url = URL(string:  baseURL + "api/patient/" + patientIDGlobal + "/contact")!
        
        //let parameters = ["latitude": latitude, "longitude": longitude, "radius": radius]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response)
            
            
            switch response.result {
            case .success:
                print("success")
                if let result = response.result.value as? NSArray{
                    print(type(of: result))
                    if(result.count < 0){
                        contactCreated = true
                    }
                    for i in 0..<result.count where i < 3{
                        print(result[i])
                        print(self.getEmergencyContact(contactID: String(describing: result[i]), id: i))
//                        if(i == 1){
//                            self.emergencyContactButton()
//                        }
                        print("after")
                    }
//                    for item in result{ // loop through patientList items
//                        print(item)
//                    }
                }
                //Transfering the responce to JSON and assigning the value
                
                
                
            case .failure:
                print("Get Geofence Failure")
                //                // create the alert
                //                let alert = UIAlertController(title: "Login Error", message: "Invalid username or password", preferredStyle: UIAlertControllerStyle.alert)
                //
                //                // add an action (button)
                //                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                //
                //                // show the alert
                //                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getEmergencyContact(contactID: String, id: Int) -> String {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        var firstNames : String = ""
        
        let url = URL(string:  baseURL + "api/patient/" + patientIDGlobal + "/contact/" + contactID)!
        
        print(url)
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                let result = response.result.value as! NSDictionary
                
                print(result["first-name"] as! String)
                firstNames = result["first-name"] as! String
                if(id == 1){
                    self.emergencyContactButton(name: firstNames)
                }
                
                print(type(of: response.result.value))
                
                print("success")
                
                
            case .failure:
                print("Failure to Get Emergency Contact")
            }
        }
        return (firstNames)
    }
}
