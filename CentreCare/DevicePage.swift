//
//  DevicePage.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/6/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit

class DevicePage: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func AddDeviceSelected(_ sender: Any) {
        
        // create the alert
        let alert = UIAlertController(title: "Unavailable", message: "Feature Disabled", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        return

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
