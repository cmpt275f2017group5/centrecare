//
//  CaretakerMenu.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/5/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit

//This page will include all the menu setting of the caretaker menu page
class CaretakerMenu: UIViewController, UITextFieldDelegate{


    @IBOutlet weak var addPatientButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addPatientButton.isHidden = false
    }
    //The will lead them to the pairing page, in the caretaker side, they will generate the QR code
    @IBAction func AddPatientAction(_ sender: UIButton) {        
        let storyBoard: UIStoryboard = UIStoryboard(name: "PatientScreen", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "QRCodeGeneratorPage") as! QRCodeGeneratorPage
        self.present(newViewController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}


