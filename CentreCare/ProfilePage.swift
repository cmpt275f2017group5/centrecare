//
//  ProfilePage.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/17/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material
import SwiftHEXColors
import Alamofire

//Set the propery of the save profile button
struct PatientProfileButtonLayout {
    struct saveProfile {
        static let width: CGFloat = 64
        static let right: CGFloat = 20
        static let height: CGFloat = 44
        static let offsetY: CGFloat = -280
    }
}

//The page displays the profile page a new user has to fill out after creating an account
//Currently does not save the profiles
class ProfilePage: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate {
    
    //Sets the textfeild variables.
    fileprivate var emailField: TextField!
    fileprivate var firstnameField: TextField!
    fileprivate var lastnameField: TextField!
    fileprivate var phonenumberField: TextField!
    fileprivate var streetField:TextField!
    fileprivate var postalcodeField:TextField!
    fileprivate var cityField:TextField!
    fileprivate var provinceField:TextField!
    fileprivate var countryField:TextField!
    
    fileprivate var email : String = "";
    fileprivate var firstname : String = "";
    fileprivate var lastname : String = "";
    fileprivate var phonenumber : String = "";
    fileprivate var street : String = "";
    fileprivate var city : String = "";
    fileprivate var postalcode : String = "";
    fileprivate var country : String = "";
    fileprivate var province : String = "";

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Sets the background color of the page
        view.backgroundColor = UIColor(hex: 0xa8c3e2)
        
        //Load all caretaker profile information from database
        loadProfileValues()
        
        //Save button that pushes changes to database
        preparesaveProfileButton()
        
        
        //This recognise the tap on the screen to clear the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UniversalLogin.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    //When the save button is tapped, this actions will occur.
    //If user choose this as caretaker account, it will go to the careataker menu
    //If user choose this as patient account, it will go to the patient menu
    func saveProfileButtonTouched(sender: UIButton){
        addProfileAlamofire()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

//Most of the functions, it clear the code to have the functions seperate
extension ProfilePage {
    
    //This will close the keyboard
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //The next couple of functions, sets the properties of the textfields on the page
    fileprivate func prepareEmailField(email: String) {
        emailField = ErrorTextField()
        emailField.placeholder = "Email"
        emailField.detail = "Error, incorrect email"
        emailField.isClearIconButtonEnabled = true
        
        emailField.text = email
        
        emailField.delegate = self
        
        emailField.keyboardType = UIKeyboardType.emailAddress
        view.layout(emailField).center(offsetY: -emailField.height - 115).left(20).right(20)
    }
    
    fileprivate func preparefirstnameField(firstname: String) {
        firstnameField = ErrorTextField()
        firstnameField.placeholder = "First Name"
        firstnameField.detail = "Error, incorrect first name"
        firstnameField.isClearIconButtonEnabled = true
        
        firstnameField.clearButtonMode = .whileEditing
        
        firstnameField.text = firstname
        
        firstnameField.delegate = self
        
        firstnameField.keyboardType = UIKeyboardType.namePhonePad
        view.layout(firstnameField).center(offsetY: -firstnameField.height - 235).left(20).right(20)
    }
    
    fileprivate func preparelastnameField(lastname: String) {
        lastnameField = ErrorTextField()
        lastnameField.placeholder = "Last Name"
        lastnameField.detail = "Error, incorrect last name"
        lastnameField.isClearIconButtonEnabled = true
        
        lastnameField.text = lastname
        
        lastnameField.delegate = self
        
        lastnameField.keyboardType = UIKeyboardType.namePhonePad
        view.layout(lastnameField).center(offsetY: -lastnameField.height - 175).left(20).right(20)
    }
    
    fileprivate func preparephonenumberField(phonenumber: String) {
        phonenumberField = ErrorTextField()
        phonenumberField.placeholder = "Phone Number"
        phonenumberField.detail = "Error, incorrect phone number"
        phonenumberField.isClearIconButtonEnabled = true
        
        phonenumberField.text = phonenumber
        
        phonenumberField.delegate = self
  
        phonenumberField.keyboardType = UIKeyboardType.phonePad
        view.layout(phonenumberField).center(offsetY: -phonenumberField.height - 60).left(20).right(20)
    }
    fileprivate func preparestreetField(street: String) {
        streetField = ErrorTextField()
        streetField.placeholder = "Street"
        streetField.detail = "Error: "
        streetField.isClearIconButtonEnabled = true
        
        streetField.text = street
        
        streetField.delegate = self

        streetField.keyboardType = UIKeyboardType.default
        view.layout(streetField).center(offsetY: -streetField.height + 0).left(20).right(20)
    }
    
    fileprivate func preparepostalcodeField(postalcode: String) {
        postalcodeField = TextField()
        postalcodeField.placeholder = "Postal Code"
        postalcodeField.isClearIconButtonEnabled = true
        
        postalcodeField.text = postalcode
        
        postalcodeField.delegate = self
 
        postalcodeField.keyboardType = UIKeyboardType.default
        view.layout(postalcodeField).center(offsetY: -postalcodeField.height + 50).left(20).width(150)
    }
    
    fileprivate func preparecityField(city: String) {
        cityField = TextField()
        cityField.placeholder = "City"
        cityField.isClearIconButtonEnabled = true
        
        cityField.text = city
        
        cityField.delegate = self

        cityField.keyboardType = UIKeyboardType.default
        view.layout(cityField).center(offsetY: -cityField.height + 50).right(20).width(180)
    }
    fileprivate func prepareprovinceField(province: String) {
        provinceField = TextField()
        provinceField.placeholder = "Province"
        provinceField.isClearIconButtonEnabled = true
        
        provinceField.text = province
        
        provinceField.delegate = self
        
        provinceField.keyboardType = UIKeyboardType.default
        view.layout(provinceField).center(offsetY: -provinceField.height + 100).left(20).width(150)
    }
    fileprivate func preparecountryField(country: String) {
        countryField = TextField()
        countryField.placeholder = "Country"
        countryField.isClearIconButtonEnabled = true
        
        countryField.text = country
        
        countryField.delegate = self
    
        countryField.keyboardType = UIKeyboardType.default
        view.layout(countryField).center(offsetY: -countryField.height + 100).right(20).width(180)
    }
    
    //This set the properties of the button on the page
    fileprivate func preparesaveProfileButton() {
        let button = RaisedButton(title: "Save", titleColor: .white)
        button.pulseColor = .white
        button.backgroundColor = Color.blue.lighten1
        button.addTarget(self, action: #selector(saveProfileButtonTouched), for: .touchUpInside)
        
        view.layout(button)
            .width(PatientProfileButtonLayout.saveProfile.width)
            .right(PatientProfileButtonLayout.saveProfile.right)
            .height(PatientProfileButtonLayout.saveProfile.height)
            .center(offsetY: PatientProfileButtonLayout.saveProfile.offsetY)
    }
    
    func loadProfileValues(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        if(userType == "CARETAKER"){
            userType = "caretaker"
        }
        
        let url = URL(string:  baseURL + "api/" + userType + "/" + userID + "/profile")!
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response.response?.allHeaderFields)
            print(response)

            switch response.result {
            case .success:

                //Transfering the responce to JSON and assigning the value

                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    let address = JSON["address"] as! NSDictionary

                    self.email = JSON["email"] as! String!
                    self.firstname = JSON["first-name"] as! String!
                    self.lastname = JSON["last-name"] as! String!
                    self.phonenumber = JSON["phone-no"] as! String!
                    self.city = address["city"] as! String!
                    self.country = address["country"] as! String!
                    self.postalcode = address["postal-code"] as! String!
                    self.province = address["province"] as! String!
                    self.street = address["street"] as! String!

                    
                    self.prepareEmailField(email: self.email)
                    self.preparefirstnameField(firstname: self.firstname)
                    self.preparelastnameField(lastname: self.lastname)
                    self.preparephonenumberField(phonenumber: self.phonenumber)
                    self.preparestreetField(street: self.street)
                    self.preparecityField(city: self.city)
                    self.preparepostalcodeField(postalcode: self.postalcode)
                    self.preparecountryField(country: self.country)
                    self.prepareprovinceField(province: self.province)


                }

            case .failure:
                // create the alert
                let alert = UIAlertController(title: "Login Error", message: "Invalid username or password", preferredStyle: UIAlertControllerStyle.alert)

                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))

                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    //This page is suppose to take a userID and use that to save the profile of the account
    //Sends profile updated profile information back to the database
    func addProfileAlamofire(){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Access-Token": sessionId
        ]
        
        if(userType == "CARETAKER"){
            userType = "caretaker"
        }
        
    
        if(userType == "patient"){
           let parameters : [String: Any] = [
                "first-name": firstnameField.text,
                "last-name": lastnameField.text,
                "phone-no": phonenumberField.text,
                "date-of-birth": 00000000,
                "address": [
                    "street": streetField.text,
                    "city": cityField.text,
                    "province": provinceField.text,
                    "country": countryField.text,
                    "postal-code": postalcodeField.text
                ]
            ]
            
            let url = URL(string:  baseURL + "api/" + userType + "/" + userID + "/profile")!
            Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                //Sends the put request
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "PatientMenu") as! PatientMenu
                self.present(newViewController, animated: true, completion: nil)
            }
            
        }
        else{
            let parameters : [String: Any] = [
                "first-name": firstnameField.text,
                "last-name": lastnameField.text,
                "phone-no": phonenumberField.text,
                "email": emailField.text,
                "address": [
                    "street": streetField.text,
                    "city": cityField.text,
                    "province": provinceField.text,
                    "country": countryField.text,
                    "postal-code": postalcodeField.text
                ]
            ]
            
            let url = URL(string: "http://centrecare.org/api/" + userType + "/" + userID + "/profile")!
            Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                //Sends the put request
                let storyBoard: UIStoryboard = UIStoryboard(name: "PatientList", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "PatientList") as! PatientList
                self.present(newViewController, animated: true, completion: nil)
            }
        }
    }
    
    
}



