//
//  CaretakerPatientMenu.swift
//  CentreCare
//
//  Created by Adnan Syed on 11/28/17.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit

class CaretakerPatientMenu: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func EmegencyContactsAction(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "EmergencyContact", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "EmergencyContactList") as! EmergencyContactList
        self.present(newViewController, animated: true, completion: nil)
    }
    
    @IBAction func PatientProfilePage(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "PatientList", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PatientProfileInfo") as! PatientProfileInfo
        self.present(newViewController, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
