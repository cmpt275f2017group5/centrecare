//
//  EmergencyContactProfile.swift
//  CentreCare
//
//  Created by Swimm Chan on 2017-11-24.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material

class EmergencyContactProfile: UIViewController {
    
    var passedInfo: [String] = EntryArray
    var currentInfo: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        currentInfo = passedInfo
        let First = UILabel()
        let Last = UILabel()
        let PhoneNumber = UILabel()
        let Address = UILabel()
        let Relationship = UILabel()
        
        First.text = currentInfo[0]
        Last.text = currentInfo[1]
        PhoneNumber.text = currentInfo[2]
        //Address.text = currentInfo[3]
        //Relationship.text = currentInfo[4]
        
        First.font = UIFont (name: "Helvetica Neue", size: 20)
        view.layout(First).center(offsetY: -90).left(155).right(20)
        
        Last.font = UIFont (name: "Helvetica Neue", size: 20)
        view.layout(Last).center(offsetY: -30).left(155).right(20)
        
        PhoneNumber.font = UIFont (name: "Helvetica Neue", size: 20)
        view.layout(PhoneNumber).center(offsetY: 30).left(185).right(20)
        
//        Address.font = UIFont (name: "Helvetica Neue", size: 20)
//        view.layout(Address).center(offsetY: 90).left(130).right(20)
        
//        Relationship.font = UIFont (name: "Helvetica Neue", size: 20)
//        view.layout(Relationship).center(offsetY: 160).left(155).right(20)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
