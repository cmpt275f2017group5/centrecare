//
//  EditThirdEC.swift
//  CentreCare
//
//  Created by Swimm Chan on 2017-11-30.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material

class EditThirdEC: UIViewController {
    
    fileprivate var FirstNameField: TextField!
    fileprivate var LastNameField: TextField!
    fileprivate var PhoneNumberField: TextField!
    fileprivate var AddressField: TextField!
    fileprivate var RelationshipField: TextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        prepareFirstNameField()
        prepareLastNameField()
        preparePhoneNumberField()
        prepareAddressField()
        prepareRelationshipField()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func EditThirdECButton(_ sender: Any) {
        if(FirstNameField.text != ""){
            ThirdEC[0] = FirstNameField.text!
        }
        if(LastNameField.text != ""){
            ThirdEC[1] = LastNameField.text!
        }
        if(PhoneNumberField.text != ""){
            ThirdEC[2] = PhoneNumberField.text!
        }
        if(AddressField.text != ""){
            ThirdEC[3] = AddressField.text!
        }
        if(RelationshipField.text != ""){
            ThirdEC[4] = RelationshipField.text!
        }
    }
    
    

}

extension EditThirdEC{
    
    fileprivate func prepareFirstNameField(){
        FirstNameField = TextField()
        FirstNameField.placeholder = "Enter First Name"
        FirstNameField.font = UIFont (name: "Helvetica Neue", size: 20)
        FirstNameField.clearButtonMode = .whileEditing
        
        view.layout(FirstNameField).center(offsetY: -60).left(20).right(20)
        
    }
    
    fileprivate func prepareLastNameField(){
        LastNameField = TextField()
        LastNameField.placeholder = "Enter Last Name"
        LastNameField.font = UIFont (name: "Helvetica Neue", size: 20)
        
        LastNameField.clearButtonMode = .whileEditing
        
        view.layout(LastNameField).center(offsetY: 0).left(20).right(20)
        
    }
    
    fileprivate func preparePhoneNumberField(){
        PhoneNumberField = TextField()
        PhoneNumberField.placeholder = "Enter Phone Number"
        PhoneNumberField.font = UIFont (name: "Helvetica Neue", size: 20)
        
        PhoneNumberField.clearButtonMode = .whileEditing
        
        view.layout(PhoneNumberField).center(offsetY: 60).left(20).right(20)
    }
    
    fileprivate func prepareAddressField(){
        AddressField = TextField()
        AddressField.placeholder = "Enter Address"
        AddressField.font = UIFont (name: "Helvetica Neue", size: 20)
        
        AddressField.clearButtonMode = .whileEditing
        
        view.layout(AddressField).center(offsetY: 120).left(20).right(20)
    }
    
    fileprivate func prepareRelationshipField(){
        RelationshipField = TextField()
        RelationshipField.placeholder = "Enter Relationship with patient"
        RelationshipField.font = UIFont (name: "Helvetica Neue", size: 20)
        
        RelationshipField.clearButtonMode = .whileEditing
        
        view.layout(RelationshipField).center(offsetY: 180).left(20).right(20)
    }
    
    
    
}

