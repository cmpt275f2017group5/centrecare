//
//  WatchEmergencyContacts.swift
//  CentreCare
//
//  Created by Swimm Chan on 2017-11-30.
//  Copyright © 2017 CenterCare. All rights reserved.
//

import UIKit
import Material

class WatchEmergencyContacts: UIViewController {
    
    let FirstEC = UILabel()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        if (EntryArray.count != 0){
            FirstEC.text = EntryArray[0]
            FirstEC.font = UIFont(name: "Helvetica Neue", size: 5)
            view.layout(FirstEC).center(offsetY: -20).left(20).right(20)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
